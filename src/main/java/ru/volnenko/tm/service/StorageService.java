package ru.volnenko.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import ru.volnenko.tm.repository.ProjectRepository;
import ru.volnenko.tm.repository.RepositoryWrapper;
import ru.volnenko.tm.repository.TaskRepository;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

public class StorageService {

    private final ProjectRepository projectRepository;
    private final TaskRepository taskRepository;

    public StorageService(ProjectRepository projectRepository, TaskRepository taskRepository)
    {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public int saveToXml()
    {
        StringWriter writer = new StringWriter();
        JAXBContext repositoryWrapperClass;
        RepositoryWrapper repositoryWrapper = new RepositoryWrapper(projectRepository, taskRepository);

        try {

            repositoryWrapperClass = JAXBContext.newInstance(RepositoryWrapper.class);
            Marshaller repositoryWrapperClassMarshaller = repositoryWrapperClass.createMarshaller();
            repositoryWrapperClassMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            repositoryWrapperClassMarshaller.marshal(repositoryWrapper, writer);

        } catch (JAXBException e) {
            e.printStackTrace();
        }


        File directory = new File("data");
        if(!directory.exists()){

            directory.mkdir();
        }

        try (FileWriter fileWriter = new FileWriter("data/xmlStorage.dat", false)) {
            fileWriter.write(writer.toString());
            fileWriter.flush();

        } catch (IOException ex) {

            System.out.println(ex.getMessage());
        }

        return 0;
    }

    public int loadFromXml()
    {
        File file = new File("data/xmlStorage.dat");

        JAXBContext repositoryWrapperClass;

        RepositoryWrapper repositoryWrapper = new RepositoryWrapper();

        try {
            repositoryWrapperClass = JAXBContext.newInstance(RepositoryWrapper.class);
            Unmarshaller repositoryWrapperClassUnmarshaller = repositoryWrapperClass.createUnmarshaller();
            repositoryWrapper = (RepositoryWrapper) repositoryWrapperClassUnmarshaller.unmarshal(file);

        } catch (JAXBException e) {
            e.printStackTrace();
        }

        projectRepository.load(repositoryWrapper.getProjectRepository().findAll());
        taskRepository.load(repositoryWrapper.getTaskRepository().findAll()) ;

        return 0;
    }

    public int saveToJson()
    {
        RepositoryWrapper repositoryWrapper = new RepositoryWrapper(projectRepository, taskRepository);

        File directory = new File("data");
        if(!directory.exists()){

            directory.mkdir();
        }

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        try {
            objectMapper.writeValue(new File("data/jsonStorage.dat"), repositoryWrapper);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public int loadFromJson()
    {
        RepositoryWrapper buffRepositoryWrapper = null;

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            buffRepositoryWrapper = objectMapper.readValue(new File("data/jsonStorage.dat"), RepositoryWrapper.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        projectRepository.load(buffRepositoryWrapper.getProjectRepository().findAll());
        taskRepository.load(buffRepositoryWrapper.getTaskRepository().findAll()) ;

        return 0;
    }


}
