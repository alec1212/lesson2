package ru.volnenko.tm.controller;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.volnenko.tm.repository.ProjectRepository;
import ru.volnenko.tm.repository.RepositoryWrapper;
import ru.volnenko.tm.repository.TaskRepository;
import ru.volnenko.tm.service.ProjectService;
import ru.volnenko.tm.service.StorageService;


import java.io.*;

public class SystemController {

    private final StorageService storageService;

    public SystemController(StorageService storageService)
    {
        this.storageService = storageService;
    }

    public int displayExit() {
        System.out.println("Terminate program...");
        return 0;
    }

    public int displayError() {
        System.out.println("Error! Unknown program argument...");
        return -1;
    }

    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public int displayHelp() {
        System.out.println("version - Display application version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of commands.");
        System.out.println("save-to-xml - Save data to xml file.");
        System.out.println("load-from-xml - Load data from xml file.");
        System.out.println("save-to-json - Save data to json file.");
        System.out.println("load-from-json - Load data from json file.");
        System.out.println("exit - Terminate console application.");

        System.out.println();
        System.out.println("project-list - Display list of projects.");
        System.out.println("project-create - Create new project by name.");
        System.out.println("project-clear - Remove all projects.");
        System.out.println();
        System.out.println("task-list - Display list of tasks.");
        System.out.println("task-create - Create new task by name.");
        System.out.println("task-clear - Remove all tasks.");
        return 0;
    }

    public int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }

    public int displayAbout() {
        System.out.println("Denis Volnenko");
        System.out.println("denis@volnenko.ru");
        return 0;
    }


    public int saveToXml(ProjectRepository projectRepository, TaskRepository taskRepository) {

        storageService.saveToXml();

        System.out.println("Выполнено сохранение в файл data/xmlStorage.dat");

        return 0;
    }

    public int loadFromXml(ProjectRepository projectRepository, TaskRepository taskRepository) {

        storageService.loadFromXml();

        System.out.println("Выполнена загрузка данных из  файла data/xmlStorage.dat");

        return 0;
    }

    public int saveToJson(ProjectRepository projectRepository, TaskRepository taskRepository) {

        storageService.saveToJson();

        System.out.println("Выполнено сохранение в файл data/jsonStorage.dat");

        return 0;
    }

    public int loadFromJson(ProjectRepository projectRepository, TaskRepository taskRepository) {

        storageService.loadFromJson();

        System.out.println("Выполнена загрузка данных из  файла data/jsonStorage.dat");

        return 0;
    }

}
